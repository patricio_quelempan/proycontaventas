from contaventasAPI.viewsets import ContaventasViewset
from rest_framework import routers
#generacion de enrutamientos de vistas
router = routers.DefaultRouter()
router.register('contaventas',ContaventasViewset)
#procedimientos que se pueden hacer y correcciones que se deben de hacer 
#Localhost:p/api/contaventas/5
#GET, POST, PUT, DELETE
#list, retrive
