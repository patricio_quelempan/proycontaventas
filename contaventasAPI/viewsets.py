from rest_framework import viewsets
from .import models
from .import serializers

class ContaventasViewset(viewsets.ModelViewSet):
    queryset = models.contaventas.objects.all()
    serializer_class = serializers.ContaventasSerializers
    
#list(), retrieve(), create(), update(), destroy()