from django.db import models

# Create your models here.
#crear clase ventas,para registrar las boletas y facturas de compra
#procura respetar siempre los nombres de las variables
class contaventas(models.Model):
    numero = models.CharField(max_length=100)
    dia   = models.CharField(max_length=2, default='DEFAULT VALUE')
    documento = models.CharField(max_length=20, default='DEFAULT VALUE')
    fecha   = models.DateTimeField()
    cliente = models.CharField(max_length=100)
    neto    = models.CharField(max_length=20)
    iva     = models.CharField(max_length=20)
    total   = models.CharField(max_length=20)
    
#Create / Insert / Add - GET
#Retrieve / Fetch - GET
#Update / Edit - PUT
#Delete / Remove -DELETE
